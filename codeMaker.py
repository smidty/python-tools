import csv
from random import randint
print("This program will generate random codes\n")

alpha = str(input('Do you want Alphanumeric codes? (type \'y\' for yes or \'n\' for no): '))
numCodes = int(input('How many codes do you want: '))
numDigits = int(input('How many digits in the code: '))

if(alpha == 'y'):
    prepend = input('Please add a unique 2 digit ALPHANUMERIC ONLY prepend (cannot start with a \'0\')')
else:
    prepend = input('Please add a unique 2 digit NUMERIC ONLY prepend (cannot start with a \'0\')')

name = input('Please name this file (DONT PUT THE FILE FORMAT ON IT): ')


print("\nPlease wait, program running...")


def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)



def make_codes(numDigits):
    if(alpha == 'y'):
        codeDigit = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z"]
    else:
        codeDigit = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    code = prepend
    for i in range(numDigits):
        if(alpha == 'y'):
            code = code + codeDigit[randint(0, 30)]
        else:
            code = code + codeDigit[randint(0, 9)]

   
    return code


def check_dupes(code, lst):
    for item in lst:
        if(item == code):
            return True
    return False

lst = []
i = 0
line_count = 0
progress = "["
spacing = "                    "
fillCount = 0
rowCount = 0
while(i < numCodes):
    i = i + 1
    rowCount += 1
    fillCount += 1
    percent = round((rowCount / numCodes) * 100, 1)
    if(round((fillCount / numCodes) * 100, 0) == 5):
        fillCount = 0
        progress += '='
        if(len(spacing) == 1):
            spacing = ""
        else:
            spacing = spacing[:-1]
        print ("\r", progress, spacing, ']', percent, '%', end="")
    code = make_codes(numDigits)
    while(check_dupes(code, lst) == True):
        code = make_codes(numDigits)

    lst.append(code)

with open(name + '.csv', mode='w', newline='') as unredeemed:
    unredeemed_writer = csv.writer(unredeemed, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    line_count = 0
    progress = "["
    spacing = "                    "
    fillCount = 0
    rowCount = 0
    for item in lst:
        rowCount += 1
        fillCount += 1
        percent = round((rowCount / numCodes) * 100, 1)
        if(round((fillCount / numCodes) * 100, 0) == 5):
            fillCount = 0
            progress += '='
            if(len(spacing) == 1):
                spacing = ""
            else:
                spacing = spacing[:-1]
        print ("\r", progress, spacing, ']', percent, '%', end="")
        unredeemed_writer.writerow([str(item)])
            


print('\n\n--Generation Complete--')

print('\n A file named \'' + name + '.csv\' has been added to this local directory containing the results.')

