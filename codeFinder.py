import csv
print("This program will use File 1 to search and delete items based on what is contained in File 2\n")

file1 = input('Enter File 1 (the organized file): ')
column1 = int(input('The column\'s position that is being referenced for File 1 (type a number): '))
# column3 = int(input('The other column\'s position that is being referenced for File 1 (type a number): '))
# column5 = int(input('The other column\'s position that is being referenced for File 1 (type a number): '))
file2 = input('Enter File 2 (The disorganized file): ')
column2 = int(input('The column\'s position that is being referenced for File 2 (type a number): '))
# column4 = int(input('The other column\'s position that is being referenced for File 2 (type a number): '))
# column6 = int(input('The other column\'s position that is being referenced for File 2 (type a number): '))
match = input('Are we finding items in BOTH lists? (type y) or finding exclusives in the first list? (type n)')
duplication = input('Are we also removing duplicates? (type y or n): ')
print("\nPlease wait, program running...")
total = 0
with open(file2) as csv_file2:
    csv_reader_emails = csv.reader(csv_file2, delimiter=',')

    for totRow in csv_reader_emails:
        total += 1

with open(file2) as csv_file2:
    csv_reader_emails = csv.reader(csv_file2, delimiter=',')
    line_count = 0
    lst = []
    rowCount = 0
    rowLength = 0
    progress = "["
    spacing = "                    "
    fillCount = 0
   

    for row in csv_reader_emails:
        # Fill up the progress bar
        rowCount += 1
        fillCount += 1
        percent = round((rowCount / total) * 100, 1)
        if(round((fillCount / total) * 100, 0) == 5):
            fillCount = 0
            progress += '='
            if(len(spacing) == 1):
                spacing = ""
            else:
                spacing = spacing[:-1]
        print ("\r", progress, spacing, ']', percent, '%', end="")

       
        with open(file1) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')

            flag = False
            temp = []
            for item in csv_reader:
                temp = item
                if(item[column1] == row[column2]):
                    flag = True

            if(match == 'y'):
                if(flag == True):
                    lst.append(row)
                    line_count += 1
            else:
                if(flag == False):
                    lst.append(row)
                    line_count += 1
                        
                

    print(f'\n\nProcessed {line_count} lines.')

rawList = []

if(duplication.lower() == 'n'):
    with open('FinalizedFile.csv', mode='w', newline='') as unredeemed:
        unredeemed_writer = csv.writer(unredeemed, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for item in lst:
            unredeemed_writer.writerow(item)
            rawList.append(item)
else:
    for item in lst:
            rawList.append(item)


if(duplication.lower() == 'y'):
    dupe_flag = False
    undupe_list = []
    undupe_list.append(rawList[0])

    for item in rawList:

        for undupe_item in undupe_list:
            if(undupe_item[0].lower() == item[0].lower()):
                dupe_flag = True
                break

        if(dupe_flag == False):
            undupe_list.append(item)
                    
        dupe_flag = False   

    with open('undupedList.csv', mode = 'w', newline='') as unduped:
        unduped_writer = csv.writer(unduped, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for item in undupe_list:
            unduped_writer.writerow(item)

print('--Recapitulation Complete--')


if(duplication.lower() == 'y'):
    print('\n A file named \'undupedList.csv\' has been added to this local directory containing the results without duplicates.') 
else:
    print('\n A file named \'FinalizedFile.csv\' has been added to this local directory containing the results.')