import csv
import os

os.system('cls')
print("This program will use File 1 to search and delete items based on what is contained in File 2\n")

print("  ________                ________                ________")
print(" |********|              |&&&&&&&&|              |&&&&&&&&| ")
print(" |********|              |&&&&&&&&|              |********| ")
print(" |********|              |&&&&&&&&|              |&&&&&&&&| ")
print(" |**file**|     minus    | file2  |    equals    |*match**| ")
print(" |********|              |&&&&&&&&|              |&&&&&&&&| ")
print(" |********|              |&&&&&&&&|              |********|")
print(" |________|              |________|              |________|\n")

file1 = input('Enter File 1 (the organized file): ')
column1 = int(input('The column\'s position that is being referenced for File 1 (type a number): '))
# isRemovingDupes1 = input('Should duplicates be removed? (type \'y\' or \'n\').')
file2 = input('Enter File 2 (The disorganized file): ')
column2 = int(input('The column\'s position that is being referenced for File 2 (type a number): '))
# isRemovingDupes2 = input('Should duplicates be removed? (type \'y\' or \'n\').')

print("\nMatching in progress. Standby...")
total = 0
lhsList = []
rhsList = []
subList = []


# Fill the left hand side (lhs) list
with open(file1) as csv_file1:
    csv_reader_emails = csv.reader(csv_file1, delimiter=',')

    for totRow in csv_reader_emails:
        total += 1
        lhsList.append(totRow)

# Fill the left hand side (lhs) list
with open(file2) as csv_file2:
    csv_reader_emails = csv.reader(csv_file2, delimiter=',')

    for totRow in csv_reader_emails:
        rhsList.append(totRow)

# if (isRemovingDupes1 = 'y'):
#     lhsList = remove_dupes(lhsList)
# if (isRemovingDupes2 = 'y'):
#     rhsList = remove_dupes(rhsList)



flag = False
rowCount = 0
rowLength = 0
fillCount = 0
progress = "["
spacing = "                      ]"
for lhsItem in lhsList:

    # Just progress bar stuff here
    rowCount += 1
    fillCount += 1
    percent = round((rowCount / total) * 100, 1)
    if(round((fillCount / total) * 100, 0) == 5):
        fillCount = 0
        progress += '='
        spacing = spacing[1:]
    if(percent > 99):
        print("\r", "[=======================] 100.0 %", end="")
    else:
        print ("\r", progress, spacing, percent, '%', end="")

    ###
    #
    # Actual logic here
    #
    ###
    flag = False
    date = ""
    for rhsItem in rhsList:
        if (lhsItem[column1] == rhsItem[column2]):
            #date = rhsItem[1]
            flag = True
    if(flag == True):
        #lhsItem.append(date)
        subList.append(lhsItem)
    #else:
        #print(lhsItem[column1])

with open('FinalizedFile.csv', mode='w', newline='') as unredeemed:
    unredeemed_writer = csv.writer(unredeemed, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    for item in subList:
        unredeemed_writer.writerow(item)
print('\n A file named \'FinalizedFile.csv\' has been added to this local directory containing the results.')


# def remove_dupes(oldlist):
#     newlist = list( dict.fromkeys(oldlist))
#     return newlist
