import csv
print("This program will use File 1 to search and delete items based on what is contained in File 2\n")

print("  ________                ________                         ") 
print(" |        |              |        |                        ")           
print(" |        |              |        |                        ")          
print(" |        |              |        |                        ")        
print(" | file1  |     minus    | file2  |    equals              ")   
print(" |        |              |________|               ________ ")
print(" |        |                                      |  sub   |") 
print(" |________|                                      |________|\n")

file1 = input('Enter File 1 (the organized file): ')
column1 = int(input('The column\'s position that is being referenced for File 1 (type a number): '))
# isRemovingDupes1 = input('Should duplicates be removed? (type \'y\' or \'n\').')
file2 = input('Enter File 2 (The disorganized file): ')
column2 = int(input('The column\'s position that is being referenced for File 2 (type a number): '))
# isRemovingDupes2 = input('Should duplicates be removed? (type \'y\' or \'n\').')

# def remove_dupes(oldList, columnNumber):
#     newList = []
#     num = 0
#     flag = False
#     for oldItem in oldList:
#         if (num == 0):
#             newList.append(oldItem)
#         else:
#             for newItem in newList:
#                 #print(oldItem[columnNumber] + " " + newItem[columnNumber])
#                 if oldItem[columnNumber] == newItem[columnNumber]:
#                     flag = True
#             if (flag == False):
#                 newList.append(oldItem)
#                 print(newList[columnNumber])
#         num += 1
#         flag = False

#     return newList

print("\nSubtraction in progress. Standby...")
total = 0
lhsList = []
rhsList = []
subList = []

# Fill the left hand side (lhs) list
with open(file1) as csv_file1:
    csv_reader_emails = csv.reader(csv_file1, delimiter=',')

    for totRow in csv_reader_emails:
        total += 1
        lhsList.append(totRow)

# Fill the left hand side (lhs) list
with open(file2) as csv_file2:
    csv_reader_emails = csv.reader(csv_file2, delimiter=',')

    for totRow in csv_reader_emails:
        rhsList.append(totRow)

# if (isRemovingDupes1 == 'y'):
#     lhsList = remove_dupes(lhsList, column1)
# if (isRemovingDupes2 == 'y'):
#     rhsList = remove_dupes(rhsList, column2)



flag = False
rowCount = 0
rowLength = 0
fillCount = 0
progress = "["
spacing = "                      ]"

# Looping through each left item
for lhsItem in lhsList:

    # Just progress bar stuff here
    rowCount += 1
    fillCount += 1
    percent = round((rowCount / total) * 100, 1)
    if(round((fillCount / total) * 100, 0) == 5):
        fillCount = 0
        progress += '='
        spacing = spacing[1:]
    if(percent > 99):
        print("\r", "[=======================]", percent, "%", end="")
    else:
        print ("\r", progress, spacing, percent, '%', end="")

    ###
    #
    # Actual logic here
    #
    ###
    flag = False

    # Looping through each right item for every left item
    for rhsItem in rhsList:
        if (lhsItem[column1] == rhsItem[column2]):
            flag = True
    if(flag == False):
        subList.append(lhsItem)
 

# Write to a file with the results
with open('FinalizedFile.csv', mode='w', newline='') as unredeemed:
    unredeemed_writer = csv.writer(unredeemed, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    for item in subList:
        unredeemed_writer.writerow(item)
print('\n A file named \'FinalizedFile.csv\' has been added to this local directory containing the results.')



